# Fortnite Updater

## OBS: captcha on login, makes this solution not worth it :/
## im working on a solution with others and have a ticket with epic games.

A set of tools, put together in a specific way, to enable 

* Auto power-on (Wake-on-Lan)
* Copy autoit-script to client
* Run autoit-script with psexec
* After psexec exits, shutdown of client is initiated.

## Usage
When the Master in running, all clients should be updated mon-fri daily at 14:00 (2pm).

For debugging, you can run powershell.exe -file 'c:\fortniteupdater\FortniteUpdater.ps1'

## Getting Started

Read this whole guide once and then perform the steps.

For better understanding:

Im calling the machine, running lancache and initiating the update the "Master".
The "clients" are the other machines.

```
cd to c:\
git clone https://gitlab.com/osvn_esport/fortniteupdater.git
(this will create a folder with the project at c:\fortniteupdater)
```

## Prerequisites

Enable Wake-on-Lan in your network:

    1. Router setup

        Most consumer routers have "wake-on-lan"-support, at least our ASUS AC66U do.

        * For each client, enable the "wake-on-lan"-capability in your router. 
            - docs_images\WoL-setting_asus_router.png
        * I suggest you also lock the ip address for each client in DHCP, but i dont think it is necessary
            - docs_images\DHCP-setting_asus_router.png

### On Clients:

* **We enabled winRM on all clients.**
    1. Run 'winrm quickconfig' on every client.

* **Enable Wake-on-Lan on all clients:**

    1. BIOS on each client

        * In our Asus Bios, there are an area called Advanced -> APM -> Power-on-via-pci-e = Enable

            - OBS. Without this setting, the clients wont react to the "wake-on-lan" magic-packets.

    2. Network setting, on each client.

        * Make sure the "wake-on-lan"-setting on your "network card"-properties, are enabled
            - docs_images\windows_network_WoL_setting.png
        * Make sure the "Allow, that this unit can turn on the machine" is set. 
            - docs_images\windows_network_WoL_power_setting_allow.png

* **Enabling automatic windows login on boot for each client:**

    1. Start -> Run -> netplwiz
    2. Remove the check in 'login for specific user is required' and apply.
        - you will be asked to enter username and password for that user.

Now we can turn on the clients and they should automaticly login to windows on startup.
    - OBS. this might be a security risk for you (because anyone could turn on a client and do stuff as local.administrator)
      In our case - we accepted the risk, it is so easy nowadays, to reset the client machines windows installation.

### Epic accounts (Email accounts):
Because, you cannot log ind to Epic Launcher, with the same account on multiple clients, you have to have an account for each client.
(You could use the same account, BUT then you would have to only update one client at a time.)

To create an epic account, you will need an email pr. account. (gmail is great)
With the emails, now create the epic accounts - make sure you save accountname and password for all accounts.
    * hint: email/accountnames could the same as computernames.

### On Master:

    * Install git ( https://git-scm.com/downloads )

    * Install docker desktop ( https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe )
        - Docker desktop needs to start on startup.
        - Docker desktop needs access to c-drive.

    * Setting the Master to not sleep/hibernate ever, in the system power management settings.
## Installation

    1.  Now we install lancache on the Master
            * Insert Master ip address in C:\fortniteupdater\lancache\docker-compose.yaml
                - There are hints in the file.

    2.  Run the Powershell scripts in the 'C:\fortniteupdater\scheduled_tasks'-folder to create scheduled tasks.
            * create_scheduled_task_for_FortniteUpdater.ps1
                - We scheduled the script to run monday-friday at 14:00
                (this is to make sure Epic games, has released the update for download.)
            * create_scheduled_task_for_lancache.ps1
                - This script, is to make sure the lancache-containers are running and that the ip of the master is present in the docker-compose.yaml.

    3.  Edit the lists with client information at 'C:\fortniteupdater\runtime\lists\'
        Every json file in this directory is loaded alphabeticly, so to control which clients to run first,
        put a '1' in front of the filename.

        The examplefiles are set with:
        * 1 client in the first file (1client_list_one.json).
            - The reason we initiate the first run on only one client, is to allow this client to download updates
              from the internet, to be saved on the Master with lancache.
        * 9 in the other (2client_list_two.json).
            - These clients will now "download" their update-files from the Master (lancache). 

        If you have 30 clients, and you would limit the runs to max 10 clients, you could have 4 files,
        * 1 client (ex. filename = 1_client1.json)
        * 9 clients (ex. filename = 2_client2-10.json)
        * 10 clients (ex. filename = 3_client11-20.json)
        * 10 clients (ex. filename = 4_client21-30.json)

### Known issues
We are currently seing that, there can acure problems with login-process in the epic launcher.
The autoit-script, does what it is programmed to do, but the epic launcher login-process hangs.
Therefor it ends with a timeout for these servers, Unfortunately this is note handled in FortniteUpdater.
It can be identified in the GlobalRun.log-file.

The clients that have not updated because epic launcher hangs in the login-process / captcha, should have script failed log entry.

"09-02-2020 23:40:20 - esport1: Script failed for esport1, last exit code: 1"

## Authors

* **Martin Køhrsen**

See also the list of [contributors](https://gitlab.com/osvn_esport/fortniteupdater/-/graphs/master) who participated in this project.

## License

This project is not licensed and purely written in the interest of helping fellow volunteers in the esport-environment.

## Acknowledgments

Martin Breitner - https://www.facebook.com/itbreitner
* **Delivered the autoit-template** - great work Martin!

## TODOs

* Handle or at least log, when autoit-script hangs without having updated fortnite - experienced sometimes, that autoit-script didnt succeed with the login process in Epic launcher, causing it to hang.
    - solution done, awaiting confirmation that timeouts are logged as expected
